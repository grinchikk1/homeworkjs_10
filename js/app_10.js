"use strict";

const tabs = document.querySelectorAll(".tabs-title");
const contents = document.querySelectorAll(".tabs-content li");

tabs.forEach((tab, index) => {
  tab.addEventListener("click", () => {
    tabs.forEach((tab) => tab.classList.remove("active"));
    tab.classList.add("active");

    contents.forEach((content) => (content.style.display = "none"));
    contents[index].style.display = "block";
  });
});
